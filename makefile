LIBS=
INCS=
SRC=src/
BIN=

all:
	g++ $(SRC)spc.cpp -o $(BIN)spc -std=c++11 $(LIBS) $(INCS)

run: all
	./$(BIN)spc data/grammar.txt data/basic.spl

debug:
	g++ $(SRC)spc.cpp -g -std=c++11 $(LIBS) $(INCS)
	gdb a.out core

clean:
	rm -f *.out
	rm -f ./$(BIN)spc

