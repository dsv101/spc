#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <map>
#include <iterator>
#include <regex>
#include <algorithm>
#include <set>
#include <stack>
#include <tuple>
#include <exception>

#define EXIT_BAD_ARGUMENTS 1
#define EXIT_BAD_SOURCE 2
#define EXIT_BAD_DESTINATION 3
#define EXIT_BAD_GRAMMAR 4
#define EXIT_BAD_TOKEN 5
#define EXIT_BAD_PARSE 6
#define EXIT_BAD_GRAMMAR_BUILD 7
#define EXIT_BAD_COMPILE 8

#define WRONG_NODE_TYPE(x,y) std::string("tried compile_" + x + " expecting compile_" + y + ":" + std::to_string(__LINE__))
#define CHECK_NODE_TYPE(w,y) if (w.sym != y) throw WRONG_NODE_TYPE(w.sym,y)
#define THROW_AT_LINE(x,y) throw "Line " + std::to_string(get_first_token(x).ln) + ": " + y

std::string load_file(std::string fname);
std::string ltrim(std::string s);
std::string rtrim(std::string s);
std::string trim(std::string s);
bool is_lambda(std::string s);
std::vector<std::string> explode(std::string const & s, char delim);
std::set<std::string> set_union(std::set<std::string> &s1, std::set<std::string> &s2);


//ROMAN VALUE CITATION http://www.geeksforgeeks.org/converting-roman-numerals-decimal-lying-1-3999/
int roman_char(char r)
{
	if (r == 'I')
        return 1;
    if (r == 'V')
        return 5;
    if (r == 'X')
        return 10;
    if (r == 'L')
        return 50;
    if (r == 'C')
        return 100;
    if (r == 'D')
        return 500;
    if (r == 'M')
        return 1000;
 
    return -1;
}

int roman_value(std::string &str)
{
	int res = 0;
 
    // Traverse given input
    for (int i=0; i<str.length(); i++)
    {
        // Getting value of symbol s[i]
        int s1 = roman_char(str[i]);
 
        if (i+1 < str.length())
        {
            // Getting value of symbol s[i+1]
            int s2 = roman_char(str[i+1]);
 
            // Comparing both values
            if (s1 >= s2)
            {
                // Value of current symbol is greater
                // or equal to the next symbol
                res = res + s1;
            }
            else
            {
                res = res + s2 - s1;
                i++; // Value of current symbol is
                     // less than the next symbol
            }
        }
        else
        {
            res = res + s1;
            i++;
        }
    }
    return res;
}

struct Token
{
	std::string n;
	std::string lxm;
	int ln;

	Token(std::string n="DEFAULT_TOKEN", std::string lxm="", int ln=-1) : n(n), lxm(lxm), ln(ln) { }

	std::string to_string()
	{
		return n + "(" + lxm + ") line=" + std::to_string(ln);
	}
};

struct Terminal
{
	std::string n;
	std::string pat;
	std::regex reg;

	Terminal(std::string n, std::string pat) : n(n), pat(pat), reg(pat, std::regex_constants::ECMAScript | std::regex_constants::icase) { }
	Terminal() : Terminal("","") { }

	std::string to_string()
	{
		return n + "(" + pat + ")";
	}
};

struct Production
{
	std::string lhs;
	std::vector<std::vector<std::string>> rhs;

	Production(std::string lhs, std::string rhs) : lhs(lhs)
	{
		std::istringstream iss(rhs);
		std::istream_iterator<std::string> beg(iss), end;
		std::vector<std::string> tokens = std::vector<std::string>(beg,end);
		std::vector<std::string> v;

		for (std::string s : tokens)
		{
			if (s == "|")
			{
				this->rhs.push_back(v);
				v.clear();
			}
			else
			{
				v.push_back(s);
				continue;
			}
		}

		if (v.size() > 0)
			this->rhs.push_back(v);
	}
	Production() : Production("","") { }

	std::string to_string()
	{
		std::string s = lhs + ":\n\t";

		int i = 0;
		for (std::vector<std::string> v : rhs)
		{
			s += std::to_string(i) + " -> ";

			for (std::string ss : v)
				s +=  ss + " ";

			if (++i < rhs.size())
				s += "\n\t";
		}

		return s;
	}
};

struct Item
{
	std::string lhs;
	std::vector<std::string> rhs;
	int dpos;
	Item(std::string lhs, std::vector<std::string> rhs, int dpos) : lhs(lhs), rhs(rhs), dpos(dpos)
	{
		if (this->lhs == "")
			this->lhs = "SHITTT!!";
	}

	Item() : lhs(), rhs(), dpos(0) {}

	bool operator <(const Item &i) const
	{
		if (lhs < i.lhs)
			return true;
		if (rhs < i.rhs)
			return true;
		if (dpos < i.dpos)
			return true;
		return false;
	}

	bool operator ==(const Item &i) const {return (lhs == i.lhs && rhs == i.rhs && dpos == i.dpos);}

	std::string to_string()
	{
		std::string s = lhs + " -> ";
		int i = 0;
		for (std::string ss : rhs)
		{
			if (i == dpos)
				s += " * ";
			s += " " + ss;
			++i;
		}

		if (dpos >= rhs.size())
			s += " * ";

		return s;
	}

	bool has_next_sym()
	{
		return (dpos < rhs.size());
	}

	std::string next_sym()
	{
		return (has_next_sym()) ? rhs[dpos] : "";
	}
};

struct DFAState
{
	int id ;
	bool closed;
	std::set<Item> items;
	std::map<std::string,DFAState*> tr;

	DFAState(std::set<Item> items, int id) : items(items), tr(), closed(false), id(id) {};
	DFAState(Item item,int id) : DFAState(std::set<Item> {item}, id) {}
	DFAState() : items(), tr(), closed(false), id(-1) {}

	std::string to_string(bool hideTransitions=false)
	{
		std::string s = "";

		for (Item i : items)
			s += "\n" + i.to_string();

		if (!hideTransitions)
			for (std::pair<std::string,DFAState*> p : tr)
			{
				std::string l = (p.first == "") ? "λ" : p.first;
				s += "\n" + l + " to " + std::to_string(p.second->id);
			}

		return s;
	}

};

struct ParseAction
{
	static enum TYPE {TYPE_SHIFT, TYPE_REDUCE, TYPE_GOTO, TYPE_ACCEPT, TYPE_SHIFT_REDUCE} t;

	int type;

	int dest;
	std::string str;
	int len;

	ParseAction(int type, void *data, int len=0) : type(type), len(len)
	{
		switch(type)
		{
			case TYPE::TYPE_SHIFT:
			case TYPE::TYPE_GOTO:
				dest = *(int*)data;
				break;
			case TYPE::TYPE_REDUCE:
				str = *(std::string*)data;
				break;
			case TYPE::TYPE_ACCEPT:
				break;
			case TYPE::TYPE_SHIFT_REDUCE:
			{
				std::pair<int,std::string> p = *(std::pair<int,std::string>*)data;
				dest = p.first;
				str = p.second;
				break;
			}
			default:
				str = "INVALID";
				dest = -1;
		}
	}

	ParseAction() : type(-1), dest(0), str(""), len(0) { }

	std::string to_string()
	{
		std::string s = "";

		switch(type)
		{
			case TYPE::TYPE_SHIFT:
				s += "SHIFT " + std::to_string(dest);
				break;
			case TYPE::TYPE_REDUCE:
				s += "REDUCE " + str + " len=(" + std::to_string(len) + ")";
				break;
			case TYPE::TYPE_GOTO:
				s += "GOTO " + std::to_string(dest);
				break;
			case TYPE::TYPE_ACCEPT:
				s += "ACCEPT";
				break;
			case TYPE::TYPE_SHIFT_REDUCE:
			{
				std::stringstream ss;
				ss << "SHIFT_REDUCE <|" << "SHIFT " << std::to_string(dest) << ", " << "REDUCE " << str << " len=(" << std::to_string(len) << ")" << "|>";
				s += ss.str();
				//s += "SHIFT_REDUCE <|" + sr.first.to_string() + ", " + sr.second.to_string() + "|>";
				break;
			}
			default:
				s += "INVALID?";
		}

		return s;
	}
};

struct ParseNode
{
	static enum TYPE {TYPE_TERMINAL, TYPE_NONTERMINAL} t;

	int type;
	std::vector<ParseNode> children;
	std::string sym;
	Token tok;

	ParseNode(int type=-1, void *data=nullptr) : type(type), children()
	{
		switch (type)
		{
			case TYPE::TYPE_TERMINAL:
			{
				Token t = *(Token*)data;
				tok = Token(t.n, t.lxm, t.ln);
				sym = tok.n;
				break;
			}
			case TYPE::TYPE_NONTERMINAL:
				sym = *(std::string*)data;
				break;
			default:
				sym = "INVALID_PARSENODE";
				break;
		}
	}

	void add_child(ParseNode pn)
	{
		children.push_back(pn);
	}

	std::string to_string(int tabcnt=0)
	{
		std::string tabs = "";
		for (int i = 0; i < tabcnt; ++i)
			tabs += "\t";

		std::string s = tabs;

		switch (type)
		{
			case TYPE::TYPE_TERMINAL:
				s += tabs + tok.to_string();
				break;
			case TYPE::TYPE_NONTERMINAL:
				s += tabs + sym;
				break;
		}

		s += "\n";
		for (ParseNode pn : children)
		{
			s += pn.to_string(tabcnt+1) + "\n";
		}

		return s;
	}
};

std::string format_match(std::string str)
{
	std::regex reg("\\s+");
	std::string nstr = std::regex_replace(str,reg," ");

	for (int i = 0; i < nstr.length(); ++i)
		nstr[i] = std::toupper(nstr[i]);

	return nstr;
}

std::string string_set_dmp(std::set<std::string> &on_stage)
{
	std::string str = "Stage:";
	for (std::string a : on_stage)
		str += "\n" + a;

	return str;
}

Token &get_first_token(ParseNode &pn)
{
	if (pn.tok.ln == -1)
		return get_first_token(pn.children[0]);
	else
		return pn.tok;
}

std::string get_words_from_list(ParseNode &wl)
{
	CHECK_NODE_TYPE(wl,"wordlist");

	std::string words = "";

	if (wl.children[0].sym == "wordlist")
	{
		words += get_words_from_list(wl.children[0]) + " ";
		words += get_words_from_list(wl.children[1]) + " ";
	}
	else
		words += wl.children[0].tok.lxm;

	return format_match(rtrim(words));
}

std::set<std::string> ALL_ACTORS;
std::map<int,std::string> ACT_LIST;
int CUR_ACT = 1;
int CUR_SCENE = 1;
std::map<int,int> SCENES;

struct GotoTarget
{
	static enum TYPE {TYPE_ACT,TYPE_ACT_SCENE,TYPE_WORDLIST,TYPE_SCENE} t;

	int type = -1;
	int anum = -1;
	int snum = -1;
	int ln = -1;
	std::string wordlist = "";

	GotoTarget(ParseNode &g)
	{
		CHECK_NODE_TYPE(g,"goto-target");
		ln = get_first_token(g).ln;

		if (g.children.size() == 2)
		{
			if (g.children[0].tok.n == "ACT")
			{
				type = TYPE_ACT;
				anum = roman_value(g.children.back().tok.lxm);
				snum = 1;
			}
			else
			{
				type = TYPE_SCENE;
				snum = roman_value(g.children.back().tok.lxm);
				anum = CUR_ACT;
			}
		}
		else if (g.children[0].sym == "wordlist")
		{
			type = TYPE_WORDLIST;
			wordlist = get_words_from_list(g.children[0]);
		}
		else
		{
			type = TYPE_ACT_SCENE;
			anum = roman_value(g.children[1].tok.lxm);
			snum = roman_value(g.children.back().tok.lxm);
		}
	}

	std::string to_string()
	{
		std::string str = "GotoTarget:\n";

		str += "type:" + std::string((type == 0) ? "TYPE_ACT" : (type == 1) ? "TYPE_ACT_SCNE" : "TYPE_WORDLIST") + "\n";
		str += "anum:" + std::to_string(anum) + "\n";
		str += "snum:" + std::to_string(snum) + "\n";
		str += "wordlist:" + wordlist + "\n";
		str += "ln:" + std::to_string(ln) + "\n";

		return str;
	}
};

std::vector<GotoTarget> GOTOS_NEEDED;


std::vector<std::string> get_actors_from_list(ParseNode &al)
{
	CHECK_NODE_TYPE(al,"actorlist");

	std::vector<std::string> actors;

	actors.push_back(format_match(al.children[0].tok.lxm));

	if (al.children[2].sym == "actorlist")
	{
		std::vector<std::string> catrs = get_actors_from_list(al.children[2]);
		for (std::string catr : catrs)
			actors.push_back(catr);
	}
	else
		actors.push_back(format_match(al.children[2].tok.lxm));

	return actors;
}

std::string compile_enterstmt(ParseNode &es, std::set<std::string> &on_stage)
{
	CHECK_NODE_TYPE(es,"enter-stmt");

	std::string code = "";

	std::vector<std::string> list;

	if (es.children[2].sym == "actorlist")
	{
		list = get_actors_from_list(es.children[2]);
	}
	else
	{
		list.push_back(format_match(es.children[2].tok.lxm));
	}

	for (std::string a : list)
	{
		if (on_stage.count(a) > 0)
			THROW_AT_LINE(es,"Actor " + a + " already on stage!");
		else
		if (ALL_ACTORS.count(a) == 0)
			THROW_AT_LINE(es,"Actor " + a + " not declared!");
		else
			on_stage.insert(a);
	}

	return code;
}

std::string compile_exitstmt(ParseNode &es, std::set<std::string> &on_stage)
{
	CHECK_NODE_TYPE(es,"exit-stmt");

	std::string code = "";

	std::vector<std::string> list;

	if (es.children[1].tok.n == "EXIT")
		list.push_back(format_match(es.children[2].tok.lxm));
	else
	{
		if (es.children[2].sym == "actorlist")
		{
			list = get_actors_from_list(es.children[2]);
		}
		else
			on_stage.clear();
	}


	for (std::string a : list)
	{
		if (on_stage.count(a) == 0)
			THROW_AT_LINE(es,"Actor " + a + " not on stage!");
		else
			on_stage.erase(a);
	}

	return code;
}

std::string compile_io(ParseNode &io, std::set<std::string> &on_stage, std::string &speaker, std::string &addressee)
{
	CHECK_NODE_TYPE(io,"io");

	std::string code = "";

	return code;
}

int compile_adj(ParseNode &a)
{
	CHECK_NODE_TYPE(a,"adj");

	if (a.children[0].tok.n == "POS_ADJ")
		return 2;
	else
		return -2;
}

int compile_adjlist(ParseNode &al)
{
	CHECK_NODE_TYPE(al,"adj-list");

	if (al.children.size() == 1)
		return compile_adj(al.children[0]);
	else
		return compile_adjlist(al.children[0]) * compile_adj(al.children[1]);
}

int compile_noun(ParseNode &n)
{
	CHECK_NODE_TYPE(n,"noun");

	if (n.children[0].tok.n == "POS_NOUN")
		return 1;
	else
		return -1;
}

int compile_value(ParseNode &v)
{
	CHECK_NODE_TYPE(v,"value");

	if (v.children.size() == 1)
		return compile_noun(v.children[0]);
	else
		return compile_adjlist(v.children[0]) * compile_noun(v.children[1]);
}

//todo: refactor for codegen!!!
int compile_arithmetic(ParseNode &a, std::set<std::string> &on_stage, std::string &speaker, std::string &addressee, std::string &code)
{
	CHECK_NODE_TYPE(a,"arithmetic");

	if (a.children.size() == 1)
	{
		if (a.children[0].tok.n == "ACTOR")
			if (ALL_ACTORS.count(format_match(a.children[0].tok.lxm)) == 0)
				THROW_AT_LINE(a,"Use of undeclared Actor: '" + format_match(a.children[0].tok.lxm) + "'");
	}
	else
	{
		if (a.children.back().sym == "arithmetic")
			compile_arithmetic(a.children.back(), on_stage, speaker, addressee, code);
		if (a.children.size() > 3 && a.children[a.children.size()-3].sym == "arithmetic")
			compile_arithmetic(a.children[a.children.size()-3], on_stage, speaker, addressee, code);
	}

	return -1;
}

std::string compile_assignment(ParseNode &a, std::set<std::string> &on_stage, std::string &speaker, std::string &addressee)
{
	CHECK_NODE_TYPE(a,"assignment");

	std::string code = "";

	int val = (a.children.back().sym == "arithmetic") ? compile_arithmetic(a.children.back(), on_stage, speaker, addressee, code) : compile_value(a.children.back());

	return code;
}

std::string compile_gototarget(ParseNode &g)
{
	CHECK_NODE_TYPE(g,"goto-target");

	std::string code = "";

	GotoTarget gt(g);

	GOTOS_NEEDED.push_back(gt);

	return code;
}

std::string compile_goto(ParseNode &g, std::set<std::string> &on_stage, std::string &speaker, std::string &addressee)
{
	CHECK_NODE_TYPE(g,"goto");

	std::string code = "";

	if (g.children.back().sym == "goto-target")
		code += compile_gototarget(g.children.back());

	return code;
}

std::string compile_stackop(ParseNode &s, std::set<std::string> &on_stage, std::string &speaker, std::string &addressee)
{
	CHECK_NODE_TYPE(s,"stackop");

	std::string code = "";

	return code;
}

std::string compile_question(ParseNode &q, std::set<std::string> &on_stage, std::string &speaker, std::string &addressee)
{
	CHECK_NODE_TYPE(q,"question");

	std::string code = "";

	return code;
}

std::string compile_sentence(ParseNode &s, std::set<std::string> &on_stage, std::string &speaker, std::string &addressee);
std::string compile_conditional(ParseNode &c, std::set<std::string> &on_stage, std::string &speaker, std::string &addressee)
{
	CHECK_NODE_TYPE(c,"conditional");

	std::string code = "";

	code += compile_sentence(c.children.back(),on_stage,speaker,addressee);

	return code;
}

std::string compile_sentence(ParseNode &s, std::set<std::string> &on_stage, std::string &speaker, std::string &addressee)
{
	CHECK_NODE_TYPE(s,"sentence");

	std::string sentence_type = s.children[0].sym;

	if ((sentence_type != "question" || s.children[0].children[1].tok.n == "YOU") && sentence_type != "conditional" && sentence_type != "goto")
		if (addressee == "" && (on_stage.size() == 1 || on_stage.size() > 2) && (sentence_type != "assignment" || s.children[0].children[0].tok.n != "I"))
		{
			THROW_AT_LINE(s,"Ambiguous statment. Who is being spoken to?\n" + string_set_dmp(on_stage));
		}

	std::string code = "";

	if (sentence_type == "io")
	{
		code += compile_io(s.children[0], on_stage, speaker, addressee);
	}
	else if (sentence_type == "assignment")
	{
		code += compile_assignment(s.children[0], on_stage, speaker, addressee);
	}
	else if (sentence_type == "goto")
	{
		code += compile_goto(s.children[0], on_stage, speaker, addressee);
	}
	else if (sentence_type == "stackop")
	{
		code += compile_stackop(s.children[0], on_stage, speaker, addressee);
	}
	else if (sentence_type == "question")
	{
		code += compile_question(s.children[0], on_stage, speaker, addressee);
	}
	else if (sentence_type == "conditional")
	{
		code += compile_conditional(s.children[0], on_stage, speaker, addressee);
	}

	return code;
}

std::string compile_sentencelist(ParseNode &sl, std::set<std::string> &on_stage, std::string &speaker, std::string &addressee)
{
	CHECK_NODE_TYPE(sl,"sentence-list");

	std::string code = "";

	if (sl.children[0].sym == "optional-interjection")
	{
		if (sl.children.size() == 3)
		{
			code += compile_sentence(sl.children[1], on_stage, speaker, addressee);
			code += compile_sentencelist(sl.children[2], on_stage, speaker, addressee);
		}
		else
		{
			code += compile_sentence(sl.children[1], on_stage, speaker, addressee);
		}
	}
	else
	{
		if (sl.children.size() == 1)
			code += compile_sentence(sl.children[0], on_stage, speaker, addressee);
		else
		{
			code += compile_sentence(sl.children[0], on_stage, speaker, addressee);
			code += compile_sentencelist(sl.children[1], on_stage, speaker, addressee);
		}

	}

	return code;
}

std::string compile_line(ParseNode &l, std::set<std::string> &on_stage)
{
	CHECK_NODE_TYPE(l,"line");

	std::string code = "";

	std::string speaker = format_match(l.children[0].tok.lxm);

	if (on_stage.count(speaker) == 0)
		THROW_AT_LINE(l,speaker + " not on stage! Stage is empty!");

	std::string addressee = "";

	if (l.children[2].sym == "optional-addressee")
		addressee = format_match(l.children[2].children[0].tok.lxm);

	if (addressee != "" && on_stage.count(addressee) == 0)
	{
		THROW_AT_LINE(l,addressee + " not on stage!\n" + string_set_dmp(on_stage));
	}

	if (addressee == "" && on_stage.size() == 2)
		for (std::string a : on_stage)
			if (a != speaker)
			{
				addressee = a;
				break;
			}


	code += compile_sentencelist(l.children.back(), on_stage, speaker, addressee);

	return code;
}

std::string compile_statement(ParseNode &s, std::set<std::string> &on_stage)
{
	CHECK_NODE_TYPE(s,"statement");

	std::string code = "";

	std::string st = s.children[0].sym;
	if (st == "enter-stmt")
	{
		code += compile_enterstmt(s.children[0], on_stage);
	}
	else if (st == "exit-stmt")
	{
		code += compile_exitstmt(s.children[0], on_stage);
	}
	else
	{
		code += compile_line(s.children[0], on_stage);
	}

	return code;
}

std::string compile_statementlist(ParseNode &sl, std::set<std::string> &on_stage)
{
	CHECK_NODE_TYPE(sl,"statement-list");

	std::string code = "";

	if (sl.children[0].sym == "statement-list")
	{
		code += compile_statementlist(sl.children[0], on_stage);
		code += compile_statement(sl.children[1], on_stage);
	}
	else
	{
		code += compile_statement(sl.children[0], on_stage);
	}

	return code;
}

std::string compile_scene(ParseNode &s, int act_num, int expected_number)
{
	CHECK_NODE_TYPE(s,"scene");

	std::string code = "";

	int val = roman_value(s.children[1].tok.lxm);
	CUR_SCENE = val;

	if (val != expected_number)
		THROW_AT_LINE(s,"Unexpected Scene " + s.children[1].tok.lxm + "(" + std::to_string(val) + ") expecting " + std::to_string(expected_number));
	else
	{
		SCENES[act_num] = val;

		std::set<std::string> on_stage;
		if (s.children.back().sym == "statement-list")
			code += compile_statementlist(s.children.back(), on_stage);
	}

	return code;
}

std::string compile_scenelist(ParseNode &sl, int act_num, int &expected_first_scene)
{
	CHECK_NODE_TYPE(sl,"scenelist");

	std::string code = "";

	if (sl.children.size() != 1 && sl.children[1].sym == "scenelist")
	{
		code += compile_scene(sl.children[0], act_num, expected_first_scene++);
		code += compile_scenelist(sl.children[1], act_num, expected_first_scene);
	}
	else
	{
		code += compile_scene(sl.children[0], act_num, expected_first_scene++);
	}

	return code;
}

std::string compile_act(ParseNode &a, int expected_number)
{
	CHECK_NODE_TYPE(a,"act");

	std::string code = "";

	int val = roman_value(a.children[1].tok.lxm);
	CUR_ACT = val;

	if (val != expected_number)
		THROW_AT_LINE(a,"Unexpected ACT " + a.children[1].tok.lxm + "(" + std::to_string(val) + ") expecting " + std::to_string(expected_number));
	else
	{
		std::string title = get_words_from_list(a.children[3]);
		ACT_LIST[val] = title;
		int expected_first_scene = 1;
		code += compile_scenelist(a.children.back(), val, expected_first_scene);
	}

	return code;
}

std::string compile_actlist(ParseNode &al, int &expected_first_act)
{
	CHECK_NODE_TYPE(al,"actlist");

	std::string code = "";

	if (al.children[0].sym == "actlist")
	{
		code += compile_actlist(al.children[0], expected_first_act);
		code += compile_act(al.children[1], expected_first_act++);
	}
	else
	{
		code += compile_act(al.children[0], expected_first_act++);
	}


	return code;
}

std::set<std::string> compile_dramatis(ParseNode &dp)
{
	CHECK_NODE_TYPE(dp,"dramatis-personae");

	std::set<std::string> actors_declared;

	if (dp.children[0].sym == "dramatis-personae")
	{
		std::set<std::string> n_actors = compile_dramatis(dp.children[0]);
		actors_declared.insert(format_match(dp.children[1].tok.lxm));

		for (std::string n_actor : n_actors)
			if (actors_declared.count(n_actor) > 0)
				THROW_AT_LINE(dp,"Duplicate Actor: " + n_actor);
			else
				actors_declared.insert(format_match(n_actor));
	}
	else
		actors_declared = std::set<std::string> {format_match(dp.children[0].tok.lxm)};

	return actors_declared;
}

bool compile_S(ParseNode &root)
{
	CHECK_NODE_TYPE(root,"S");

	int expected_first_act = 1;
	ALL_ACTORS = compile_dramatis(root.children[1]);
	
	std::string code = compile_actlist(root.children[2], expected_first_act);

	/*std::cout << "SCENES" << std::endl;
	for (std::pair<int,int> p : SCENES)
		std::cout << p.first << " " << p.second << std::endl;
	std::cout << "ACTS" << std::endl;
	for (std::pair<int,std::string> p : ACT_LIST)
		std::cout << p.first << " " << p.second << std::endl;*/

	for (int i = 0; i < GOTOS_NEEDED.size(); ++i)
	{
		GotoTarget &gt = GOTOS_NEEDED.at(i);

		bool valid = true;
		if (gt.type == GotoTarget::TYPE::TYPE_WORDLIST)
		{
			int match = -1;
			for (std::pair<int,std::string> p : ACT_LIST)
				if (p.second == gt.wordlist)
				{
					match = p.first;
					break;
				}
			if (match == -1)
				valid = false;
		}
		else
		{
			if (ACT_LIST.count(gt.anum) == 0 || SCENES[gt.anum] < gt.snum)
				valid = false;
		}

		if (!valid)
			throw "Line " + std::to_string(gt.ln) + ": Invalid goto target!\n" + gt.to_string();
	}

	return true;
}

struct DFA
{
	std::vector<DFAState*> states;
	std::map<std::string,Terminal> *terminals;
	std::vector<std::string> *prodKeys;
	std::map<std::string,Production> *productions;
	std::set<std::string> *nullables;
	std::map<std::string,std::set<std::string>> *first;
	std::map<std::string,std::set<std::string>> *follow;
	std::map<std::string,DFAState*> seen;
	std::map<int,std::map<std::string,ParseAction>> table;

	DFA(std::map<std::string,Terminal> *terminals, std::vector<std::string> *prodKeys, std::map<std::string,Production> *productions, std::set<std::string> *nullables, std::map<std::string,std::set<std::string>> *first, std::map<std::string,std::set<std::string>> *follow) :
		states(), terminals(terminals), prodKeys(prodKeys), productions(productions), nullables(nullables), first(first), follow(follow)
	{
		if (prodKeys->size() == 0)
		{
			std::cerr << "can't build dfa from nothing silly!";
			return;
		}

		add_state(new DFAState(Item("S'",std::vector<std::string> {prodKeys->at(0)},0), 0));

		int k = 0;

		while(1)
		{
			//std::cout << "ITERATION: " << k++ << "\n" << to_string() << std::endl;

			int s1 = states.size();

			for (int i = 0; i < states.size(); ++i)
			{
				DFAState *Q = states[i];

				if (Q == nullptr)
					break;

				std::map<std::string,std::set<Item>> tmp = go(Q->items);
				for (std::pair<std::string,std::set<Item>> p : tmp)
				{
					//std::cout << "\nGO ON " << p.first << std::endl;
					//for (Item i : p.second)
					//{
					//	std::cout << i.to_string() << std::endl;
					//}

					DFAState *ndfas = new DFAState(p.second, states.size());

					if (seen.count(ndfas->to_string()) > 0)
					{
						add_transition(p.first,Q,seen[ndfas->to_string()]);
						delete ndfas;
					}
					else
					{
						add_state(ndfas);
						add_transition(p.first,Q,ndfas);
					}
				}
			}

			if (s1 == states.size())
				break;
		}

		//build_table();
	}

	void build_table()
	{
		for (int i = 0; i < states.size(); ++i)
		{
			table[i] = std::map<std::string,ParseAction>();

			for (std::pair<std::string,DFAState*> tr : states.at(i)->tr)
			{
				int sid = 0;
				for (DFAState *d : states)
				{
					if (d == tr.second)
						break;
					++sid;
				}

				if (productions->count(tr.first) > 0)
				{
					
					table[i][tr.first] = ParseAction(ParseAction::TYPE::TYPE_GOTO, &sid);
				}
				else
				{
					table[i][tr.first] = ParseAction(ParseAction::TYPE::TYPE_SHIFT, &sid);
				}
			}

			for (Item item : states.at(i)->items)
			{
				if (!item.has_next_sym())
				{
					if (item.lhs == "S'")
						table[i]["$"] = ParseAction(ParseAction::TYPE::TYPE_ACCEPT,&(item.lhs),item.rhs.size());
					else
					{
						std::set<std::string> *f = &(follow->at(item.lhs));
						for (std::string s : *f)
						{
							ParseAction np = ParseAction(ParseAction::TYPE::TYPE_REDUCE, &(item.lhs),item.rhs.size());

							if (table[i].count(s) != 0 && table[i][s].to_string() != np.to_string())
							{
								if (table[i][s].type == ParseAction::TYPE::TYPE_SHIFT && np.type == ParseAction::TYPE::TYPE_REDUCE)
								{
									std::pair<int,std::string> *sr = new std::pair<int,std::string>(table[i][s].dest, np.str);
									np = ParseAction(ParseAction::TYPE::TYPE_SHIFT_REDUCE, sr, np.len);
									delete sr;
								}
								else
									throw std::string("TABLE CONFFLICT! table[" + std::to_string(i) + "][" + s + "]\nOLD:\n" + table[i][s].to_string() + "\nNEW:\n" + np.to_string() + "\n");
							}
							else
								table[i][s] = np;
						}
					}
					//table[i]["$"] = (item.lhs != "S'") ? ParseAction(ParseAction::TYPE::TYPE_REDUCE,&(item.lhs),item.rhs.size()) : ParseAction(ParseAction::TYPE::TYPE_ACCEPT,&(item.lhs),item.rhs.size());
				}
			}


			//std::cout << i << ":" << std::endl;

			//for (std::pair<std::string,ParseAction> p : table[i])
			//	std::cout << p.first << " => " << p.second.to_string() << std::endl;
		}
	}

	std::string table_to_string()
	{
		std::string s = "";

		for (std::pair<int,std::map<std::string,ParseAction>> p : table)
		{
			s += std::to_string(p.first) + "|";
			for (std::pair<std::string,ParseAction> pp : p.second)
				s += pp.first + "=>" + pp.second.to_string() + ",";
			s += "\n"; 
		}

		return s;
	}

	void add_state(DFAState *Q)
	{
		if (Q == nullptr)
		{
			std::cerr << "tried adding null state to dfa" << std::endl;
			return;
		}

		//std::cout << "adding state \n" << Q->to_string() << std::endl;

		closure(Q->items);
		states.push_back(Q);
		seen[Q->to_string()] = Q;
	}

	void add_transition(std::string on, DFAState *from, DFAState *to)
	{
		if (on == "" || is_lambda(on) || from == nullptr || to == nullptr)
		{
			std::cerr << "bad add_transition" << std::endl;
			return;
		}

		//std::cout << "adding transition on " << on << " from \n\n" << from->to_string(true) << "\n\n to \n\n" << to->to_string(true) << std::endl;

		from->tr[on] = to;
	}

	std::map<std::string,std::set<Item>> go(std::set<Item> items)
	{
		std::map<std::string,std::set<Item>> go;

		for (Item i : items)
		{
			if (i.has_next_sym() && !is_lambda(i.next_sym()))
			{
				go[i.next_sym()].insert(Item(i.lhs,i.rhs,i.dpos+1));
				closure(go[i.next_sym()]);
			}
		}

		return go;
	}

	void closure(std::set<Item> &closure)
	{
		//std::set<Item> closure = items;

		do
		{
			int s1 = closure.size();

			for (Item i : closure)
			{
				if (i.has_next_sym() && productions->count(i.next_sym()) > 0)
				{
					for (std::vector<std::string> p : productions->at(i.next_sym()).rhs)
						closure.insert(Item(i.next_sym(),(!is_lambda(p.at(0))) ? p : std::vector<std::string>(),0));
				}
			}

			if (s1 == closure.size())
				break;
		} while (1);
	}

	~DFA()
	{
		for (DFAState *d : states)
			if (d != nullptr)
				delete d;
	}

	std::string to_string()
	{
		std::string s = "";
		int i = 0;
		for (DFAState *d : states)
			s += "\n" + std::to_string(i++) + ":\n#################" + d->to_string() + "\n#################";

		return s;
	}

	bool parse(std::vector<Token> input, ParseNode &tree)
	{
		if (input.size() == 0)
			return false;

		bool accept = false;
		std::stack<std::pair<std::string,int>> s;
		std::stack<ParseNode> pns;
		s.push(std::pair<std::string,int>("$",0));
		int ct = 0; //current token index
		int cs = 0; //current state index

		do
		{
			std::string csym = input[ct].n;
			cs = s.top().second;

			if (table.count(cs) == 0)
			{
				std::cout << "Reached unknown state " << cs << std::endl;
				return false;
			}

			if (table[cs].count(csym) == 0)
			{
				std::cout << "Unexpected token " << ct << " of '" << input[ct].to_string() << "'" << std::endl;
				return false;
			}

			ParseAction pa = table[cs][csym];
			int action = pa.type;

			if (pa.type == ParseAction::TYPE::TYPE_SHIFT_REDUCE)
				if (follow->at(pa.str).count(input[ct+1].n) == 0)
					action = ParseAction::TYPE::TYPE_SHIFT;
				else
					action = ParseAction::TYPE::TYPE_REDUCE;

			switch (action)
			{
				case ParseAction::TYPE::TYPE_SHIFT:
					s.push(std::pair<std::string,int>(csym,pa.dest));
					pns.push(ParseNode(ParseNode::TYPE::TYPE_TERMINAL, &(input[ct])));
					++ct;
					break;
				case ParseAction::TYPE::TYPE_REDUCE:
				{
					csym = pa.str;
					ParseNode npn(ParseNode::TYPE::TYPE_NONTERMINAL, &(csym));

					std::vector<ParseNode> npns;

					for (int i = 0; i < pa.len; ++i)
					{
						s.pop();
						npns.push_back(pns.top());
						pns.pop();
					}

					for (int i = npns.size()-1; i >= 0; --i)
						npn.add_child(npns[i]);

					pns.push(npn);

					if (table.count(s.top().second) == 0 || table[s.top().second].count(csym) == 0)
					{
						std::cout << "Reduce, but no goto for '" << csym << "' table[" << s.top().second << "][" << csym << "]..." << std::endl;
						return false;
					}

					s.push(std::pair<std::string,int>(csym, table[s.top().second][csym].dest));
					break;
				}
				case ParseAction::TYPE::TYPE_ACCEPT:
					tree = pns.top();
					accept = true;
					++ct;
					break;
			}

		} while (ct < input.size());

		return true;
	}
};

struct Grammar
{
	std::vector<std::string> termKeys;
	std::vector<std::string> prodKeys;
	std::map<std::string,Terminal> terminals;
	std::map<std::string,Production> productions;
	std::set<std::string> nullables;
	std::map<std::string,std::set<std::string>> first;
	std::map<std::string,std::set<std::string>> follow;

	DFA  *dfa;

	Grammar(std::string fname) : termKeys(), prodKeys(), terminals(), productions(), nullables(), first(), follow()
	{
		std::regex reg("(.*)->(.*)");

		bool mt = true;
		std::string g = load_file(fname);

		std::istringstream iss(g);
		for (std::string line; std::getline(iss, line);)
		{
			if (line == "")
			{
				mt = false;
				continue;
			}

			std::smatch m;
			if (std::regex_search(line, m, reg) && m.size() > 2)
			{
				std::string l = trim(m.str(1));
				std::string r = trim(m.str(2));

				if (mt)
				{
					Terminal t(l, r);
					terminals[t.n] = t;
					termKeys.push_back(t.n);
				}
				else
				{
					Production p(l, r);

					if (productions.find(p.lhs) == productions.end())
					{
						productions[p.lhs] = p;
						prodKeys.push_back(p.lhs);
					}
					else
						for (std::vector<std::string> v : p.rhs)
							productions[p.lhs].rhs.push_back(v);
				}
			}
		}

		std::set<std::string> on;
		do
		{
			on = nullables;
			for (std::string k : prodKeys)
				if (nullables.count(k) == 0)
					for (std::vector<std::string> v : productions[k].rhs)
					{
						bool b = false;
						for (std::string s : v)
						{
							if (!is_lambda(s) && nullables.count(s) == 0)
							{
								b = true;
								break;
							}
						}
						if (!b)
						{
							std::set<std::string> nset;
							nset.insert(k);
							nullables = set_union(nullables, nset);
						}
					}
					bool all = true;
		} while (nullables != on);

		std::map<std::string,std::set<std::string>> of;
		for (std::string k : termKeys)
			first[k] = std::set<std::string> {k};
		while (first != of)
		{
			of = first;
			for (std::string k : prodKeys)
				for (std::vector<std::string> v : productions[k].rhs)
						for (std::string s : v)
						{
							first[k] = set_union(first[k], first[s]);
							if (nullables.count(s) == 0)
								break;
						}
		}

		std::map<std::string,std::set<std::string>> _f;
		follow[prodKeys[0]] = std::set<std::string> {"$"};
		while (follow != _f)
		{
			_f = follow;

			for (std::string k : prodKeys)
				for (std::vector<std::string> v : productions[k].rhs)
					for (int i = 0; i < v.size(); ++i)
					{
						std::string x = v.at(i);
						if (productions.count(x) > 0)
						{
							bool b = false;
							for (int j = i+1; j < v.size(); ++j)
							{
								std::string y = v.at(j);
								follow[x] = set_union(follow[x], first[y]);
								if (nullables.count(y) == 0 || is_lambda(y))
								{
									b = true;
									break;
								}
							}
							if (!b)
								follow[x] = set_union(follow[x], follow[k]);
						}
					}
		}

		//std::cout << "Grammar built" << std::endl;

		dfa = new DFA(&terminals, &prodKeys, &productions, &nullables, &first, &follow);
	}

	~Grammar()
	{
		if (dfa != nullptr)
			delete dfa;
	}

	std::string to_string()
	{
		std::string s = "Terminals:\n\n";

		for (std::string ss : termKeys)
			s += terminals[ss].to_string() + "\n";


		s += "\nProductions:\n\n";

		for (std::string ss : prodKeys)
			s += productions[ss].to_string() + "\n";

		s += "\nNullables:\n\n";

		for (std::string ss: nullables)
			s += ss + "\n";

		s += "\nFirsts:\n\n";

		for (std::string ss: prodKeys)
		{
			s += ss + ":";
			for (std::string sss : first[ss])
				s += "\n\t" + sss;

			s += "\n";
		}

		s += "\nFollows:\n\n";

		for (std::string ss: prodKeys)
		{
			s += ss + ":";
			for (std::string sss : follow[ss])
				s += "\n\t" + sss;

			s += "\n";
		}

		if (dfa != nullptr)
			s += "\nDFA:\n\n" + dfa->to_string();

		return s;
	}

	std::vector<Token> tokenize(std::string s)
	{
		int l = 1;
		std::vector<Token> ts;

		while (s != "")
		{
			bool bad = true;
			std::smatch m;
			for (std::string k : termKeys)
			{
				if (std::regex_search(s, m, terminals[k].reg, std::regex_constants::match_continuous) && m.size() > 0)
				{
					std::string lxm = m.str();
					ts.push_back(Token(k,lxm,l));
					s.erase(0,lxm.length());

					l += std::count(lxm.begin(), lxm.end(), '\n');
					for (int i = s.find_first_not_of("\n"); i >= 0; --i)
						if (s[i] == '\n')
							++l;

					s = ltrim(s);
					bad = false;
					break;
				}
			}

			if (bad && s != "")
			{
				if (s.length() > 5)
					s = s.substr(0,5);

				std::string ex = "Bad token on line ";
				ex += std::to_string(l);
				ex += " around \"";
				ex += s;
				ex += "\"...";

				throw ex;
				break;
			}
		}

		ts.push_back(Token("$","$",l));

		return ts;
	}
};

int main(int argc, const char * argv[])
{
	if (argc < 3)
	{
		std::cout << "Useage: spc GRAMMAR{.txt} SOURCE{.spl}" << std::endl;
		return EXIT_BAD_ARGUMENTS;
	}

	std::fstream fs(argv[2]);
	if (!fs.good())
	{
		std::cout << "Can not access '" << argv[2] << "'." << std::endl;
		return EXIT_BAD_SOURCE;
	}

	std::string source = load_file(argv[2]).c_str();

	//std::cout << "Source:" << std::endl << source << std::endl << std::endl;

	Grammar *g = new Grammar(argv[1]);

	try
	{
		//std::cout << "building table" << std::endl;
		g->dfa->build_table();
		//std::cout << "parse table built" << std::endl;
	}
	catch (std::string s)
	{
		std::cout << s << std::endl;
		return EXIT_BAD_GRAMMAR_BUILD;
	}

	//std::cout << "Grammar:" << std::endl << g->to_string() << std::endl << std::endl;

	std::vector<Token> tokens;

	try
	{
		tokens = g->tokenize(source);
	}
	catch (std::string s)
	{
		std::cout << s << std::endl;
		if (g != nullptr)
			delete g;
		return EXIT_BAD_TOKEN;
	}

	/*std::cout << "Tokens:" << std::endl;

	int i = 0;
	for (Token t : tokens)
	{
		std::cout << i << ": " << t.to_string() << std::endl;
		++i;
	}*/

	ParseNode tree;
	if (g->dfa->parse(tokens, tree) == true)
	{
		std::cout << "parse succeeded" << std::endl;
		//std::cout << tree.to_string() << std::endl;

		std::ofstream of(std::string(argv[2]) + ".pt");
		of << tree.to_string() << std::endl;
		of.close();

		try
		{
			compile_S(tree);
		}
		catch (std::string s)
		{
			std::cout << "compile failed!" << std::endl << s << std::endl;
			if (g != nullptr)
				delete g;
			return EXIT_BAD_COMPILE;
		}
		

		if (g != nullptr)
			delete g;
		return 0;
	}
	else
	{
		std::cout << "parse failed" << std::endl;
		if (g != nullptr)
			delete g;
		return EXIT_BAD_PARSE;
	}
}

std::string load_file(std::string fname)
{
	std::string s = "";
	std::ifstream fs(fname, std::ios::in);
	
	if (!fs.is_open())
	{
		std::cout << "File: '" << fname << "' not found!" << std::endl;
		return s;
	}
	
	std::string line = "";
	while (!fs.eof())
	{
		std::getline(fs, line);
		s += line + "\n";
	}
	
	fs.close();
	return s;
}

std::string ltrim(std::string s)
{
	std::string whitespaces (" \t\f\v\n\r");

	std::size_t found = s.find_first_not_of(whitespaces);
	if (found!=std::string::npos)
		s.erase(0,found);
	else
		s.clear();

	return s;
}

std::string rtrim(std::string s)
{
	std::string whitespaces (" \t\f\v\n\r");

	std::size_t found = s.find_last_not_of(whitespaces);
	if (found!=std::string::npos)
		s.erase(found+1);
	else
		s.clear();

	return s;
}

std::string trim(std::string s)
{
	return ltrim(rtrim(s).c_str());
}

bool is_lambda(std::string s)
{
	return (s == "λ" || s  == "lambda" || s == "ε" || s == "epsilon");
}

std::vector<std::string> explode(std::string const & s, char delim)
{
    std::vector<std::string> result;
    std::istringstream iss(s);

    for (std::string token; std::getline(iss, token, delim); )
    {
        result.push_back(std::move(token));
    }

    return result;
}

std::set<std::string> set_union(std::set<std::string> &s1, std::set<std::string> &s2)
{
	std::set<std::string> s;

	for (std::string ss : s1)
		s.insert(ss);

	for (std::string ss : s2)
		s.insert(ss);

	return s;
}