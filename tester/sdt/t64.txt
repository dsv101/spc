Correct conditional test.

Romeo, conducts the action.
Juliet, two.
Young Cato, y.
Nobleman, n.
Nurse, newline.

Act I: Setup.
Scene I: Setting up the variables.

[Enter Young Cato and Nobleman]

Young Cato: Thou are as lovely as the sum of a furry good meet sunny fancy safe
    peach and the sum of a sweet ingenious charming peach and the sum of
    an invulnerable amazing lad and a zany castle.

Nobleman: You art the sum of an admirable good attractive proud yellow mighty
    town and the sum of a pretty golden amazing black aunt and the sum of
    a noiseless handsome fine choir and a cat! 

[Exit Nobleman]
[Enter Nurse]

Young Cato: You are the sum of a rich smooth bedazzled grace and a zany goat!

Scene II: We ask if Juliet is not equal to negative one.

[Enter Romeo and Juliet]

Romeo: 
    You are a beautiful queen. 
    Are you different from a toad? 
    If so, let us proceed posthaste to Act I, Scene III.
    If not, let us proceed posthaste to Act I, Scene IV.

Romeo: Recall that we should never get here!

[Exeunt]

Scene III: Prints Y and then exits the program.

[Enter Romeo, Nurse and Young Cato]

Romeo: Young Cato, speak your mind.
Romeo: Nurse, speak your mind. Let us return.

[Exit Romeo]

Scene IV: Prints N and then exits the program.

[Enter Romeo, Nurse and Nobleman]

Romeo: Nobleman, speak your mind. 
Romeo: Nurse, speak your mind. Let us return.

[Exit Romeo]
