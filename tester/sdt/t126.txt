nine three This is correct number seven four.

Romeo, nine four.
Juliet, nine five.
Tybalt, nine six.
Benvolio, nine seven.
Lady Macbeth, nine eight.
Doctor Butts, nine nine.
Bottom, one zero zero.
All the Goths, one zero one.
All Conspirators, one zero two.
Bastard of Orleans, one zero three.
Cinna the Poet, one zero four.
Henry VIII, one zero five.
Horatio, one zero six.
Henry IV, who does nothing.
Henry VI, nosepicker.
Old Man, one zero seven.
Old Gobbo, one zero eight.
The Ghost, one zero nine.
Vaux, one one zero.
Thomas Wart, one one one.

Act I: one one two.

Scene I: one one three.

[Enter Henry VIII and Thomas Wart ]

Thomas Wart: You are as ugly as thrice Henry VIII!

[Exit Thomas Wart ]
[Enter Benvolio ]

Henry VIII: You are as beautiful as twice Henry VIII.

[Exeunt]

[Enter Cinna the Poet and Romeo]
Romeo: You are as good as the sum of a fancy amazing king and a queen.

[Exeunt]

[Enter Vaux]
Vaux: I am the sum of a king and a fancy lad.
[Exit Vaux]

[Enter Old Man]
Old Man: I am a toad.
[Exit Old Man]

[Enter All the Goths]
All the Goths: I am the sum of a king and a fancy fancy fancy lad.
[Exeunt]

[Enter Romeo]
Romeo: I am the sum of All the Goths and All the Goths.
[Exit Romeo]

Scene II: one one four.

[Enter Horatio and Romeo ]

Romeo: You are as vile as the square of Cinna the Poet. Open your heart.
    
[Exeunt Horatio and Romeo ]
[Enter Tybalt and Bottom ]

Bottom: You are as good as the sum of Tybalt and Old Man!

[Exeunt]

Act II: one one five.

Scene I: one one six.

[Enter Benvolio and Cinna the Poet ]

Benvolio: You are as beautiful as the difference between
    Cinna the Poet and Old Gobbo!

[Exeunt Benvolio and Cinna the Poet ]
[Enter Bastard of Orleans and Thomas Wart ]

Thomas Wart: You are as bad as the difference between Doctor Butts and Vaux.

[Exeunt]

Scene II: one one seven.

[Enter Old Gobbo and Romeo ]

Old Gobbo: You are as vile as thrice Thomas Wart. Open your heart.

[Exeunt Old Gobbo and Romeo ]
[Enter Tybalt and Doctor Butts ]

Tybalt: You are as bad as half Tybalt.

[Exeunt]

Act III: one one eight.

Scene I: one one nine.

[Enter Juliet and Doctor Butts ]

Juliet: I am a fancy amazing queen.
    You are as bad as the quotient of Henry VIII and Juliet.

[Exeunt Juliet and Doctor Butts ]
[Enter Henry VIII and Old Man ]

Henry VIII: You are as bad as the sum of Bottom and Henry VIII!

[Exeunt]

Scene II: one two zero.

[Enter The Ghost and Horatio ]

The Ghost: You are as ugly as the square of All the Goths! Open your heart.

[Exeunt The Ghost and Horatio ]
[Enter Doctor Butts and Old Man ]

Old Man: You are as vile as the quotient of Cinna the Poet and Juliet.

[Exeunt]

Act IV: one two one.

Scene I: one two two.

[Enter Henry VIII and Bastard of Orleans ]

Bastard of Orleans: You are as vile as the square root of Old Gobbo.

[Exit Henry VIII ]
[Enter Lady Macbeth ]

Lady Macbeth: You are as ugly as the difference between Horatio and Tybalt.

[Exeunt]

Scene II: one two three.

[Enter The Ghost and Thomas Wart ]

The Ghost: You are as vile as half Vaux!

[Exeunt The Ghost and Thomas Wart ]
[Enter Benvolio and Old Gobbo ]

Old Gobbo: You are as ugly as the cube of Bottom.

[Exeunt]

Act V: one two four.

Scene I: one two five.



[Enter Benvolio and Doctor Butts ]

Benvolio: You are as bad as the sum of Thomas Wart and The Ghost.

[Exeunt Benvolio and Doctor Butts ]

[Enter All the Goths and Doctor Butts ]

All the Goths: You are as old as a king.

[Exeunt]

[Enter Tybalt and Vaux ]

Tybalt: You are as bad as the sum of yourself and the quotient of Romeo and Doctor Butts!

[Exeunt]

Scene II: one two six.

[Enter The Ghost and Juliet ]

Juliet: You are as good as half Cinna the Poet.

[Exeunt The Ghost and Juliet ]
[Enter Benvolio and Bottom ]

Benvolio: You are as bad as the sum of Romeo and Bastard of Orleans!

[Exeunt]

Act VI: one two seven.

Scene I: one two eight.

[Enter Tybalt and Bottom ]

Tybalt: You are as beautiful as the cube of All the Goths.

[Exit Tybalt ]
[Enter The Ghost ]

The Ghost: You are as ugly as half Lady Macbeth.

[Exeunt]

Scene II: one two nine.

[Enter Juliet and Romeo ]

Romeo: You are as beautiful as twice Benvolio.

[Exit Juliet ]
[Enter Lady Macbeth ]

Lady Macbeth: You are as ugly as the difference between Bastard of Orleans and The Ghost!

[Exeunt]

Act VII: one three zero.

Scene I: one three one.

[Enter Tybalt and Juliet ]

Tybalt: You are as bad as the product of Old Man and Old Man.

[Exeunt Tybalt and Juliet ]
[Enter All the Goths and Doctor Butts ]

Doctor Butts: You are as vile as the sum of Old Man and Bottom.
All the Goths: You are as old as a king.

[Exeunt]

Scene II: one three two.

[Enter All the Goths and Tybalt ]

All the Goths: You are as beautiful as the sum of a king and the cube of Romeo! Open your heart.

[Exeunt All the Goths and Tybalt ]
[Enter Cinna the Poet and Bastard of Orleans ]

Cinna the Poet: You are as good as half Cinna the Poet. Open your heart.

[Exeunt]

Act VIII: one three three.

Scene I: one three four.

[Enter Benvolio and Bottom ]

Bottom: You are as beautiful as the sum of Juliet and Horatio.

[Exeunt Benvolio and Bottom ]
[Enter Tybalt and Romeo ]

Romeo: You are as vile as the square root of Vaux! Open your heart.

[Exeunt]

Scene II: one three five.

[Enter The Ghost and Tybalt ]

Tybalt: You are as good as the product of Henry VIII and Juliet.

[Exeunt The Ghost and Tybalt ]
[Enter Lady Macbeth and Doctor Butts ]

Doctor Butts: You are as good as the cube of Cinna the Poet! Open your heart.

[Exeunt]

Act IX: one three six.

Scene I: one three seven.

[Enter Vaux and Doctor Butts ]

Doctor Butts: You are as beautiful as thrice All Conspirators!

[Exeunt Vaux and Doctor Butts ]
[Enter Benvolio and All the Goths ]

Benvolio: You are as good as the difference between Doctor Butts and Lady Macbeth.

[Exeunt]

Scene II: one three eight.

[Enter Benvolio and Cinna the Poet ]

Benvolio: You are as vile as the sum of yourself and the cube of Vaux. Open your heart.

[Exeunt Benvolio and Cinna the Poet ]
[Enter All the Goths and Tybalt ]

Tybalt: You are as good as the difference between Henry VIII and Bottom!

[Exeunt]

Act X: one three nine.

Scene I: one four zero.

[Enter Henry VIII and Bastard of Orleans ]

Bastard of Orleans: You are as beautiful as the opposite of All Conspirators.
    Open your heart.

[Exit Henry VIII ]
[Enter Tybalt ]

Tybalt: You are as bad as half Juliet.

[Exeunt]

Scene II: one four one.

[Enter The Ghost and Cinna the Poet ]

The Ghost: You are as beautiful as the sum of yourself 
    and the sum of Horatio and Vaux!
    

[Exeunt The Ghost and Cinna the Poet ]
[Enter Horatio and Thomas Wart ]

Thomas Wart: You are as vile as the cube of Old Man! 

[Exeunt]

Act XI: one four two.

Scene I: one four three.

[Enter Horatio and Old Man ]

Horatio: 
    You are as good as Cinna the Poet.  Open your heart.
    You are as bad as the square root of Cinna the Poet.

[Exeunt Horatio and Old Man ]
[Enter Vaux and All Conspirators ]

Vaux: You are as beautiful as the sum of Benvolio and Doctor Butts!

[Exeunt]

Scene II: one four four.

[Enter Lady Macbeth and Thomas Wart ]

Lady Macbeth: You are as ugly as the product of Henry VIII and Old Man.

[Exit Lady Macbeth ]
[Enter Horatio ]

Horatio: You are as bad as the sum of Romeo and Benvolio!

[Exeunt]

