This is a correct count up from zero to ten using a stack.

Romeo, the counter.
Tybalt, the newline.

Act I: Acting.

Scene I: Main.

[Enter Romeo]

Romeo: Romeo, you are a toad. Remember thyself.
    I am an amazing attractive beautiful lad. I am the sum of myself and
    a noble roman.
    
[Exit Romeo]

Scene II: Setting up the newline.

[Enter Tybalt]

Tybalt: I am Romeo.

[Exit Tybalt]


Scene III: The Loop.

[Enter Romeo and Tybalt]

Tybalt: Remember thyself. You are the sum of yourself and a toad.
    Are you as good as nothing? If not, let us proceed posthaste
    to Act I, Scene III.
    
[Exeunt]

Scene IV: The big finish.

[Enter Romeo and Tybalt]

Tybalt: Open your heart.
Romeo: Speak your mind. 
Tybalt: Recall that it is nobler to have loved and lost than
    to never have loved at all. Are you worse than nothing?
    If not, let us proceed posthaste to Act I, Scene IV.
    
[Exeunt]
